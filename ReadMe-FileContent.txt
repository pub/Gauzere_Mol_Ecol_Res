#####	Codes-Script Article MolEcolRes	######

- "code C++" directory : contains all the C++ codes (.cpp, .o, .hpp). NB: the parameters defining the mating system properties of the ancestral and current populations (alpha, Npc) are declared in the "Simul.cpp" code.
A Code::Blocks project (.cbp) is also provided as a possibility to generate the exe file on different platforms
Note that several functions from the Boost library are used in the C++ code

-  "wrapper6.py" : python script that defines almost all the input parameters (Nfam, Noff, Nrepet, Nloci...). The script also implements the random draw of the allelic frequencies. The compiled C++ code is called from the Python application.

- "Simul.exe" : exemple of compiled C++ code

- "Script_AsREML_Method_PhiAmatrix.as" : AsREML script to analyse the simulated maternal progenies using an animal model