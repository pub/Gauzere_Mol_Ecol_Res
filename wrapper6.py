import subprocess
import os
import sys
import math
import shutil
import glob
from numpy.random.mtrand import dirichlet

# template infile
template = """sigmaA #%f
sigmaE #%f
Number of loci quanti #%d
Number of alleles by locus quanti #%d
Number of individuals #%d
Number of families #%d
Number of offspring #%d
Number of loci #%d
Selfing rate #%f
Number of generations #1
Number of alleles by locus #%d
Allele frequencies %s
Allele frequencies quanti %s
"""
# defines values to iterate over
IsigmaA = 0.02,
IsigmaE = 0.9,
IlociQuanti = 500,
IallByLocusQuanti = 5,
INpop = 200,
INfam = 15,
INoff = 40,
Iloci = 20,
IallByLocus = 10,
Iselfing = 0,
Nrepet = 50

CMD = '.\Simul.exe'

# initialize counter
NMAX = Nrepet*len(IsigmaA)*len(IsigmaE)*len(INpop)*len(INfam)*len(INoff)*len(IlociQuanti)*len(IallByLocusQuanti)*len(Iloci)*len(IallByLocus)*len(Iselfing)
C = 0
print "jusque la ok"
# supprimer les vieux fichiers
vieuxtrucs = glob.glob('D:\Documents and Settings\Gayl\Mes documents\INRA 2009\Programmes\Simul_hybmatrix\dir*')
print len(vieuxtrucs)
for i in range(1,len(vieuxtrucs)+1):
    path = './dir%s' %(i)
    print path
    if os.path.isdir(path):
        shutil.rmtree(path)
# iterates
for sigmaA in IsigmaA:
    for sigmaE in IsigmaE:
        for lociQuanti in IlociQuanti:
            for allByLocusQuanti in IallByLocusQuanti:
                for Npop in INpop:
                    for Nfam in INfam:
                        for Noff in INoff:
                            for loci in Iloci:
                                frequences = []
                                for allByLocus in IallByLocus:
                                    if allByLocus<5: alpha = (4*Npop*10**(-4) )/(allByLocus-1)
                                    else: alpha = (4*Npop*10**(-3) )/(allByLocus-1)
                                    for selfing in Iselfing:
                            
                                        for repet in range(Nrepet):

                                # generates the input file

                                            print len(frequences)
                                            while len(frequences)<loci:
                                                sfreqs = dirichlet([alpha] * allByLocus)
                                                test = 0
                                                for i in sfreqs:
                                                    if i>0.001: test=1+test                                      
                                                if test == allByLocus:
                                                    sfreqs=['#%f' %j for j in sfreqs]
                                                    sfreqs = ' '.join(sfreqs)
                                                    frequences.append(sfreqs)
                                                    print sfreqs
                                            print frequences
                                            print "jusque la ok 3"
                                    
                                            sQuantiFreqs = [1./allByLocusQuanti]*allByLocusQuanti
                                            sQuantiFreqs = ['#%.3f' %i for i in sQuantiFreqs]
                                            sQuantiFreqs = ' '.join(sQuantiFreqs)
                            
                                            infile = template%(
                                            sigmaA,sigmaE,lociQuanti,allByLocusQuanti,
                                            Npop,Nfam,Noff,loci, selfing, allByLocus,
                                            frequences, sQuantiFreqs)
                
                                            C+=1
                                                    
                                            print '[%d/%d]' %(C,NMAX), sigmaA,sigmaE,Npop,Nfam,Noff,lociQuanti,allByLocusQuanti,loci, allByLocus,selfing
                
                                            f = open('infile.txt','w')
                                            f.write(infile)
                                            f.close()
                            
                                            # runs the program
                                            popen = subprocess.Popen((CMD,'infile.txt'),
                                            stdout = subprocess.PIPE,
                                            stderr = subprocess.PIPE)
                                            stdout, stderr = popen.communicate()

                                            if len(stderr) != 0:
                                                sys.exit('ERROR!!!\n%s' %stderr)

                                            print 'jusque la ca va'
                                
                                            # saves the output in a new directory
                                            chemin = 'dir%s' %(str(C))
                                            os.mkdir(chemin)
                                            print '2'

                                            for fname in ['phenodata.asd','simul.ped','infile.txt',
                                                  'paramfile.txt','loiselfile.txt','loiselpatfile.txt','genofile.txt',
                                                  'AddValues.txt','phiAfile.txt','genofileQuant.txt']:
                                                os.rename(fname, os.path.join(chemin, fname))
                                            del frequences[:]
                             
                            

