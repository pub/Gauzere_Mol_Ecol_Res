!LOGFILE !QUIET !WORKSPACE 200
simulations to estimate heritability from an estimated relatedness PhiA_matrix
Ind !A # 1st colomn of phenotype data = individual label
Phenotype !M0 # 2nd colomn of phenotype data = phenotypic value
PhiA_matrix.grm !NSD !DENSE # relationship matrix in dense format and non inversible
Pheno_repet1.asd !Skip1 # Data file with phenotypic values and fixed/random effects - first line is headings
Phenotype ~ mu !r giv(Ind,1) # animal model with no fixed effects

!PIN !define # define the contents of the output file - here calculates the total phenotypic variance and narrow-sense heritability
F phenvar 1 + 2
F addvar 1 * 4
H herit 1 3