#ifndef LOCUS
#define LOCUS
#include "Random.hpp"
//#include "Params.hpp"

using namespace std;


class Locus {
      public:
             Locus();  // Constructeur
             ~Locus();
       //      static int NlocQuant;
        //     float AllelicEffect;
        //     static int NallQuant;
            static float sigmaA;
          //  static int NlocQuant;
            static Random* random;
             void SetAllelicEffect(int allele);
             float GetAllelicEffect() const;

    private:
          //   int NlocQuant, NallQuant;
          //  Params *NallQuant;
            float AllelicEffect;
             };


#endif
