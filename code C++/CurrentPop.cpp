/********************************************************
*Classe CurrentPop: qui cr�e la pop de base servant aux                  *
*simulations. La fonction Generation effectue un cycle                  *
*de reproduction. La fonction Freqall calcule les                              *
*fr�quences all�liques � chaque g�n�ration. La matrice                   *
*d'apparentement "vrai" et estim� (Loiselle) sont                              *
*calcul�es et mises � jour.                                                                     *
**********************************************************/

#include<iostream>
#include<fstream>
#include <math.h>
#include "CurrentPop.hpp"
#include "Locus.hpp"
#include "Params.hpp"
#include <cstdlib>

void dirichlet(int npop, std::vector <double>& poids,
               boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tirage, boost::math::gamma_distribution<>& gamma){

    // Fonction qui calcule les proba cumul�es d'un vecteur distribu� suivant une Dirichlet
    // Les param�tres de la Dirichlet sont port�s par la loi gamma qui entre en param�tre
    // Le vecteur a une dimension n+1 et commence syst�matiquement par un 0
    // Le vecteur n'est pas renormalis�, ie la somme totale contenue dans la derni�re case n'est pas �gale � 1

    //double tot=0;
    poids[0]=0;
    for (int k=0; k<npop; k++)
        {
            poids[k+1] = poids[k] + quantile(gamma, tirage());
            //tot+=poids[k];
        }
    //for (int k=0; k<npop; k++) {poids[k] /= tot;}
	}

int draw_from_vec(vector<double>& cumprob,
                   boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tir)
    // On entre un vecteur de proba cumulees de dimension n+1 avec 0 en 0 et un max en n
    // On entre un g�n�rateur de nombre aleatoire q uniforme entre 0 et 1
    // La fonction sort un entier entre 0 et n-1 tire suivant les proba
    {
        vector<double> cumprob2 = cumprob;
        double randu;

    randu = cumprob[cumprob.size()-1]*tir();
    for (int k=0; k<cumprob.size(); k++) {cumprob2.push_back(cumprob[cumprob.size()-1]);};
    int high=1;
    int low=1;
    int mid;
    if (cumprob2[1] < randu)  {
        while (cumprob2[high]<randu) {low=high; high *=2;}
        while ((high-low)>1) {
            mid=boost::math::round((high+low)/2);
            if (cumprob2[mid]<randu) {low=mid;}
            else {high=mid;}
        }}
    return high-1;
    }


CurrentPop::CurrentPop() {  // SDM: mis en private pour verifier qu 'il ne sert pas (pas important)
     length=0; // SDM: ajoute cette ligne
     nullall=0;//ajout le 09/06/08
     Npop=0;
     Nfam=0;
     Noff=0;
     s=0;
     Nloc=0;
       sigmaA=0;
       sigmaE=0;
       sigmaM=0;    //12_Nov_2013
     KinshipMatrix=0;
     tpKinshipMatrix=0;
     KinFinal=0;
     FreqMatrix=0;
     MargSum=0;
     FreqMatrixQuant=0;
     MargSumQuant=0;

     NlocQuant=0;
   //  FreqAllQuant=0;
    lengthQuant=0;
    popCache = NULL;
    Ngeneration=0;
}
/* LG: CurrentPop est une classe (d�finie ds CurrentPop.hpp) mais aussi une fonction membre (ou methode) de la classe CurrentPop*/
/* LG: fonction d�finie ici sans param () et avec des valeurs initiales a 0*/

//int CurrentPop::Ngeneration = 0;

CurrentPop::CurrentPop(int Ngeneration, int Npop, int Nfam, int Noff,float s,int Nloc,vector<vector< float > > Freq,Random* random,float sigmaA,float sigmaE ,float sigmaM ,int NlocQuant/*,float FreqAllQuant*/,vector<float> FreqQuant ){
     int i,j,k;
     lengthQuant=FreqQuant.size();
    /* SDM: length est institue membre pour connaitre le nombre d'alleles */
     this->random=random; /*LG: "this" est un pointeur*/
     this->Npop=Npop;
     this->Nfam=Nfam;
     this->Noff=Noff;
     this->Nloc=Nloc;
     this->s=s;
       this->sigmaA=sigmaA;
       this->sigmaE=sigmaE;
       this->sigmaM=sigmaM;
     this->NlocQuant=NlocQuant;
     NallQuant=lengthQuant;
    this->Ngeneration=Ngeneration;
    g = 0; // compteur de generations
/* SDM: ces fichiers restent ouverts pendant tout l'execution du programme ?
   si c'est le cas et qu'ils deviennent gros, il est possible de vider le tampon d'ecriture regulierement
   vider le tampon ( = flush ) ca signifie ecrire physiquement sur le disque ce qui a ete passe dans write( )
   il ne faut pas multiplier les acces disques, mais d'un autre cote il ne faut pas accumuler trop de donnees dans la memoire vive
   a priori le probleme ne vient pas d'un exces d'utilisation de la memoire mais du nombre d'operation (qund je dis probleme, c'est une facon de parler)
*/
     phenodata.open("phenodata.asd");
     if(!phenodata.is_open()) {cerr<<"cannot create phenodata\n";exit(-1);}
     AddValues.open("AddValues.txt");
     if(!AddValues.is_open()) {cerr<<"cannot create AddValues\n";exit(-1);}

     genofileQuant.open("genofileQuant.txt");
     if(!genofileQuant.is_open()) {cerr<<"cannot create genofileQuant\n";exit(-1);}
     genofileQuant<<"Indiv"<<"\t";
     for (int i=0;i<NlocQuant;i++) genofileQuant << "Loc quanti" << i+1 <<"\t";
     genofileQuant << "\n";

     pop= new Indiv*[Npop]; /*LG: "pop" est un pointeur sur la classe Indiv*/
     popCache = new Indiv*[Npop];
     for(i=0;i<Npop;i++)  {
         pop[i]=new Indiv;
         popCache[i] = NULL;
     }

// ************************************************************************* NX PHENOTYPES ***********
// LG_28/07: tirage des genotypes quanti
    vector<int> vecQuant;
    for(i=0;i<NlocQuant;i++) {
        cout<<"lengthQuant a GO = "<<lengthQuant<<"\n";
        vecQuant.clear();
        for(j=0;j<lengthQuant;j++) {
            for(k=0;k<FreqQuant[j]*2*Npop;k++) vecQuant.push_back(j);
            }
      //    for(k=0;k<2*Npop;k++) vecQuant.push_back(1);
              //  for(k=0;k<FreqAllQuant*2*Npop;k++) vecQuant.push_back(1);  // les locus sont contraints a 2 alleles nomm�s 1 et 2 en freq FreqAllQuant et 1-FreqAllQuant
              //  for(k=0;k<(1-FreqAllQuant)*2*Npop;k++) vecQuant.push_back(2);
   // }
        for(j=0;j<Npop;j++) {
            int randQuant=random->irand(vecQuant.size()) ;
            pop[j]->SetGenotypesQuant(vecQuant[randQuant],0,i);
            vecQuant.erase(vecQuant.begin()+randQuant);
            int rand2Quant=random->irand(vecQuant.size());
            pop[j]->SetGenotypesQuant(vecQuant[rand2Quant],1,i);
            vecQuant.erase(vecQuant.begin()+rand2Quant);
            }
        }
     for(j=0;j<Npop;j++) {
         genofileQuant<<"Indiv"<<j+1<<"\t";
         for(i=0;i<NlocQuant;i++) {
             genofileQuant << pop[j]->GenotypeQuant[0][i]+1<<pop[j]->GenotypeQuant[1][i]+1<<"\t";
          }
         genofileQuant << "\n";
     }
     genofileQuant << "\n\n";

// ************************************************************************* NX PHENOTYPES ***********
     /*LG: boucle pour tirer les phenotypes */
 //essai avec classe Locus
      //Locus** loc;

    allelequant = new Locus**[NlocQuant];
      for(j=0;j<NlocQuant;j++) {
          allelequant[j] = new Locus* [NallQuant];
             for (k=0;k<NallQuant;k++) {
             allelequant[j][k]=new Locus;
             allelequant[j][k]->SetAllelicEffect(k);
             }
      }
 /*   float addValue[Npop],AllelicEffect[NlocQuant][lengthQuant];    //methode sans classe Locus
    int l;
    for(j=0;j<NlocQuant;j++) {
        cout<<"\n";
        for (k=0;k<lengthQuant;k++) {
            AllelicEffect[j][k]=0;
            AllelicEffect[j][k]=random->nrand(0.,sigmaA);
            cout<<"locus "<<j <<" allele "<<k <<" "<<AllelicEffect[j][k]<<" \t";
        }
    }   */
      float addValue[Npop];
      int l;
      for(i=0;i<Npop;i++) {
        addValue[i]=0;
        for(j=0;j<NlocQuant;j++) {
          // for (l=0;l<2;l++) addValue[i]+=loc[j]->AllelicEffect[j][pop[i]->GenotypeQuant[l][j]];
         //  for (l=0;l<2;l++) addValue[i]+=locus[pop[i]->GenotypeQuant[l][j]]->GetAllelicEffect();
           for (l=0;l<2;l++) addValue[i]+=allelequant[j][pop[i]->GenotypeQuant[l][j]]->GetAllelicEffect();
           pop[i]->SetbreedVal(addValue[i]);
            }
        pop[i]->SetphenVal(pop[i]->getbreedVal()+random->nrand(0.,sigmaE));
        //phenodata << i+1 << "\t" << pop[i]->getphenVal() << endl;//impression pop de base
        //AddValues << i+1 <<"\t" << pop[i]->getbreedVal() <<endl;
           // <<"\t"<<"allelic effect "<<"\t"<<AllelicEffect[j][0]<< "\t"<<AllelicEffect[j][1]<< "\t"<< pop[i]->GenotypeQuant[0][j]+1<<pop[i]->GenotypeQuant[1][j]+1<<endl;
        }
    //phenodata <<"\n\n";
    //AddValues <<"\n";

     vector<int> vecall;
     /*LG: boucle pour tirer les genotypes*/
     for(i=0;i<Nloc;i++) {
         length=Freq[i].size();
          vecall.clear();
        //liste avec 2Npop elements remplie en fonction des frequences avec l'all�le 0 ou 1 etc...
         for(j=0;j<length;j++) {
             for(k=0;k<Freq[i][j]*2*Npop;k++) vecall.push_back(j);
         }
          //pour que les individus ne soient pas forc�ment homozygotes au d�part
         for(j=0;j<Npop;j++) {
             int rand=random->irand(vecall.size()) ;
             pop[j]->SetGenotypes(vecall[rand],0,i);
             vecall.erase(vecall.begin()+rand);
             int rand2=random->irand(vecall.size());
             pop[j]->SetGenotypes(vecall[rand2],1,i);
             vecall.erase(vecall.begin()+rand2);
             }
     }

     KinshipMatrix= (float**) malloc(Npop*sizeof(float*));
     if (!KinshipMatrix) {cerr << "oom" << endl; exit(-1);}
     for (i=0; i<Npop; i++) {
		KinshipMatrix[i]= (float*) malloc(Npop*sizeof(float));
		if (!KinshipMatrix[i]) {cerr << "oom" << endl; exit(-1);}
		for (j=0; j<Npop; j++) KinshipMatrix[i][j]= 0;
		KinshipMatrix[i][i]= 0.5;
	 }

     tpKinshipMatrix= (float**) malloc(Npop*sizeof(float*));
     if (!tpKinshipMatrix) {cerr << "oom" << endl; exit(-1);}
     for (i=0; i<Npop; i++) {
		tpKinshipMatrix[i]= (float*) malloc(Npop*sizeof(float));
		if (!tpKinshipMatrix[i]) {cerr << "oom" << endl; exit(-1);}
		// pas besoin de l'initialiser
	 }

     KinFinal= (float**) malloc((Nfam*Noff)*sizeof(float*));
     if (!KinFinal) {cerr << "oom" << endl; exit(-1);}
     for (i=0; i<(Nfam*Noff); i++) {
		KinFinal[i]= (float*) malloc((Nfam*Noff)*sizeof(float));
		if (!KinFinal[i]) {cerr << "oom" << endl; exit(-1);}
     }


     FreqMatrix= (float**) malloc(Nloc*sizeof(float*));
     if (!FreqMatrix) {cerr << "oom" << endl; exit(-1);}
     for (i=0; i<Nloc; i++) {
		FreqMatrix[i]= (float*) malloc(length*sizeof(float));
		if (!FreqMatrix[i]) {cerr << "oom" << endl; exit(-1);}
		for (j=0; j<length; j++) FreqMatrix[i][j]= 0;  // SDM: on initialise pas avec Freq? je comprends pas tout encore...
     }

     MargSum= (float*) malloc(Nloc*sizeof(float));
     if (!MargSum) {cerr << "oom" << endl; exit(-1); }

     FreqMatrixQuant= (float**) malloc(NlocQuant*sizeof(float*));
     if (!FreqMatrixQuant) {cerr << "oom" << endl; exit(-1);}
     for (i=0; i<NlocQuant; i++) {
		FreqMatrixQuant[i]= (float*) malloc(lengthQuant*sizeof(float));
		if (!FreqMatrixQuant[i]) {cerr << "oom" << endl; exit(-1);}
		for (j=0; j<lengthQuant; j++) FreqMatrixQuant[i][j]= 0;  // SDM: on initialise pas avec Freq? je comprends pas tout encore...
     }

     MargSumQuant= (float*) malloc(NlocQuant*sizeof(float));
     if (!MargSumQuant) {cerr << "oom" << endl; exit(-1); }

     genofile.open("genofile.txt");
     if(!genofile.is_open()) {cerr<<"cannot create genofile\n";exit(-1);}
     for (int i=0;i<Nloc;i++) genofile << "Loc" << i+1 <<"\t";

    genofile << "\n";

     pedigree.open("simul.ped");
     if(!pedigree.is_open()) {cerr<<"cannot create pedigree\n";exit(-1);}
     pedigree << "Ind\tDam\tSire"<<endl;//lisible par Pedigree Viewer

     loiselfile.open("loiselfile.txt");
     if(!loiselfile.is_open()) {cerr<<"cannot create loiselfile\n";exit(-1);}

     loiselpatfile.open("loiselpatfile.txt");
     if(!loiselpatfile.is_open()) {cerr<<"cannot create loiselpatfile\n";exit(-1);}
    //"else {cerr<<"loiselpatfile created ok\n";exit(-1);}

     freqall.open("freqall.txt");
     if(!freqall.is_open()) {cerr<<"cannot create freqall\n";exit(-1);}

     phiAfile.open("phiAfile.txt");
     if(!phiAfile.is_open()) {cerr<<"cannot create phiAfile\n";exit(-1);}

	phiAdense.open("phiAdense.txt");
	if(!phiAdense.is_open()) {cerr<<"cannot create phiAdense\n";exit(-1);}

     acc=0;//valeur initiale du compteur
}

CurrentPop::~CurrentPop(){

    if (FreqMatrix) {
        for (int i=0; i<length; i++) {
            if (FreqMatrix) free(FreqMatrix[i]);
        }
        free(FreqMatrix);
    }
  if (FreqMatrixQuant) {
        for (int i=0; i<lengthQuant; i++) {
            if (FreqMatrixQuant) free(FreqMatrixQuant[i]);
        }
        free(FreqMatrixQuant);
    }

     int i;

    for (i=0;i<NlocQuant; i++) {
        for (int j=0; j<NallQuant; j++) {
            delete allelequant[i][j];
        }
        delete[] allelequant[i];
    }
    delete[] allelequant;

     genofileQuant.close();
     genofile.close();
     pedigree.close();
     phenodata.close();
     AddValues.close();
     loiselfile.close();
     loiselpatfile.close();
    freqall.close();
     phiAfile.close();
	phiAdense.close();

	// SDM: ajoute []... il faut ecrire delete[] quand on a cree avec new [] (ce qui n'empeche pas de delete chaque element comme tu l'as fait plus haut)
     for (i=0;i<Npop;i++) delete pop[i];
     delete [] pop;
     for (i=0;i<Npop;i++) if (popCache[i]) delete popCache[i];
     delete [] popCache;

     // SDM je libere la memoire que j'ai allouee a la place des vectors
     if (KinshipMatrix) {
		for (i=0; i<Npop; i++) if (KinshipMatrix[i]) free(KinshipMatrix[i]);
		free(KinshipMatrix);
	 }
     if (tpKinshipMatrix) {
		for (i=0; i<Npop; i++) if (tpKinshipMatrix[i]) free(tpKinshipMatrix[i]);
		free(tpKinshipMatrix);
	 }
	 if (MargSum) free(MargSum);
	 if (MargSumQuant) free(MargSumQuant);
}


inline int CurrentPop::round (float f) {
    return (int)(f + 0.5);
}


void CurrentPop::Generation(boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tirage,
                             boost::math::gamma_distribution<>& gamma, int Ngroup) {

     g++; // on incremente le compteur de generations
cout<<"compteur generations: "<<g<<"sur "<<Ngeneration<<"au total"<<endl;
     int i,j,k;
     // int Ngroup=20; // Nombre de m�res dans chaque groupe qui percoit les p�res de mani�re similaire
     int NofGroup=Npop/Ngroup+1;
     int groupMother;

     int motherlist[Npop];//liste des m�res
     int fatherlist[Npop];//liste des p�res

     std::vector <double> poidsMother (Npop+1, 0.);
     dirichlet(Npop,poidsMother, tirage, gamma);

     std::vector < std::vector <double> > poidsFather (NofGroup, std::vector <double> (Npop+1, 0.));
     for (int p=0; p<NofGroup;p++)
     {
         dirichlet(Npop,poidsFather[p], tirage, gamma);
     }


    // delete all Indiv about to become grandparents
     for (i=0;i<Npop; i++) {
         if (popCache[i]!=NULL) {
             delete popCache[i];
             popCache[i] = NULL;
         }
     }

     Indiv* tamponPop[Npop];
   //  Indiv* tamponPopF[Nfam*Noff];
    // vector<vector<float> > tmpKM;//matrice tampon dans laquelle on stocke temporairement les coeff d'apparentement
    // sDM:: je la supprime, remplace par celle au niveau de la classe
//     vector<float> Tmp(Npop,0); // SDM: a priori ca sert a rien
     bool flag;
     for(i=0;i<Npop;i++) tamponPop[i]=new Indiv;//nouvelle pop temporaire

  //   for(i=0;i<(Nfam*Noff);i++) tamponPopF[i]=new Indiv;//nouvelle pop temporaire

//     tmpKM.resize(Npop,Tmp); SDM: ca sert a rien tpKinshipMatrix est deja de la bonne taille
     acc+=Npop;//compteur incr�ment� pour sortir les individus en num�rotation absolue

//    for(b=0;b<Nfam;b++){   //boucle sur les a familles
//        if (b%Ngroup==0) {dirichlet(Npop, poids, tirage, gamma);}
//
//        for (a=0;a<Noff;a++){
//            motherlistF[b*Noff+a]=b;//tirage d'une liste donnant les indices des m�res
//            cout<<b<<"-"<<a<<"="<<b*Noff+a<<": "<<motherlistF[b*Noff+a]<<"\t";
//            if (random->uniform()<s) fatherlistF[b*Noff+a]=motherlistF[b*Noff+a];//id pour p�res
//            else {
//                //do fatherlistF[b*Noff+a]=random->irand(Npop);   // peut etre rajouter une dirichlet?
//                do fatherlistF[b*Noff+a]=draw_from_vec(poids,tirage);
//                while (fatherlistF[b*Noff+a]==motherlistF[b*Noff+a]);     // evite de tirer meme pere meme mere
//                }
//
// *** Tirage des peres et meres et calcul des breedval+phenotype ***
        for(i=0;i<Npop;i++){
            //motherlist[i]=random->irand(Npop);//tirage d'une liste donnant les indices des m�res
            motherlist[i]=draw_from_vec(poidsMother,tirage);//tirage d'une liste donnant les indices des m�res
            groupMother=motherlist[i]/Ngroup;
            if (random->uniform()<s) fatherlist[i]=motherlist[i];//id pour p�res
            else {
                //do fatherlist[i]=random->irand(Npop);
                do fatherlist[i]=draw_from_vec(poidsFather[groupMother],tirage);
                while (fatherlist[i]==motherlist[i]);
            }
        }
cout<<"OK"<<endl;
// ******************************** Tirage des genotypesQuanti******************
        for (k=0;k<NlocQuant;k++){//on boucle sur les locus en premier
          for (i=0;i<Npop;i++) {//sur les individus
               tamponPop[i]->SetGenotypesQuant(pop[motherlist[i]]->GenotypeQuant[random->irand(2)][k],0,k);
               tamponPop[i]->SetGenotypesQuant(pop[fatherlist[i]]->GenotypeQuant[random->irand(2)][k],1,k);
            }
        }
// ************************************************************************* NX PHENOTYPES ***********
//******************************************************************************************
// *** Tirage des genotypes des i indiv aux k locus ***
        for (k=0;k<Nloc;k++){//on boucle sur les locus en premier
          for (i=0;i<Npop;i++) {//sur les individus
               tamponPop[i]->SetGenotypes(pop[motherlist[i]]->Genotype[random->irand(2)][k],0,k);
               tamponPop[i]->SetGenotypes(pop[fatherlist[i]]->Genotype[random->irand(2)][k],1,k);
             //on fixe les all�les du g�ne k de l'ind i en fonction des all�les parentaux (un de ses chromosomes tir� au hasard)
         }
        }
// *** teste si les individus de pop ont �t� parents et supprime ceux qui ne l'ont pas �t�  ***
cout<<"OK2"<<endl;
        for (i=0;i<Npop;i++) {
        // SDM: flag pas initialise, objets JAMAIS detruits!
            flag= 0;
            for(j=0;j<Npop;j++) if (i==motherlist[j]||i==fatherlist[j]) flag=1;
            if (!flag) delete pop[i];
            else popCache[i] = pop[i];
        }
// *** fournit les pointeurs Pere et Mere ***
        for (i=0;i<Npop;i++) {
          tamponPop[i]->Mere=pop[motherlist[i]];
          tamponPop[i]->Pere=pop[fatherlist[i]];
          tamponPop[i]->Indexfather=fatherlist[i];
          tamponPop[i]->Indexmother=motherlist[i];
        }

// *** edition du PEDIGREE (dans simul.ped) avec num�rotation absolue des individus et sans 0 (ASReml) ***
        for(i=0;i<Npop;i++) pedigree << i+1+acc << "\t" << motherlist[i]+acc-Npop+1 <<"\t"<< fatherlist[i]+acc-Npop+1 <<endl;

// *** copie les individus tamponPop dans pop  // SDM: remarque: on copie les adresses uniquement ***
        for (i=0;i<Npop;i++) pop[i]=tamponPop[i];
cout<<"OK3"<<endl;

        float addValue;
        int l;
        for(i=0;i<Npop;i++) {
            addValue=0;
            for(j=0;j<NlocQuant;j++) {
                for (l=0;l<2;l++) addValue+=allelequant[j][pop[i]->GenotypeQuant[l][j]]->GetAllelicEffect();
                pop[i]->SetbreedVal(addValue);
                }
            pop[i]->SetphenVal(pop[i]->getbreedVal()+random->nrand(0.,sigmaE));
            //phenodata << i+1 << "\t" << pop[i]->getphenVal() << endl;//impression pop de base
            //AddValues << i+1 <<"\t" << pop[i]->getbreedVal()<<endl;
        }
        //phenodata <<"\n\n";
        //AddValues <<"\n";

//****************************************************************************************
// *** calcul des coeff d'apparentements entre individus i et j ***

        for (i=0;i<Npop;i++) {
          for(j=0;j<Npop;j++) {
              if (i!=j && j>i) {// on remplit ici les cases hors de la diagonale
              tpKinshipMatrix[i][j]=0.25*(KinshipMatrix[pop[i]->Indexfather][pop[j]->Indexmother]+KinshipMatrix[pop[i]->Indexfather][pop[j]->Indexfather]+KinshipMatrix[pop[i]->Indexmother][pop[j]->Indexmother]+KinshipMatrix[pop[i]->Indexmother][pop[j]->Indexfather]);
              }
              else if (j<i)  tpKinshipMatrix[i][j]= tpKinshipMatrix[j][i];
              //cases de la diagonale
              else  tpKinshipMatrix[i][j]=0.5*(1+KinshipMatrix[pop[i]->Indexfather][pop[i]->Indexmother]);
              //on sort le phi(i,i) qui vaut 1/2(1+Fi) Fi coeff d'inbreeding
              }
          }
        for (i=0;i<(Npop);i++) {
          for(j=0;j<(Npop);j++) {
              KinshipMatrix[i][j]=/* SDM: ici*/ tpKinshipMatrix[i][j];
              phiAfile << KinshipMatrix[i][j]<<"\t";
          }
          phiAfile <<"\n";
        }
cout<<"OK4"<<endl;
// *** edition PHENODATA et AddValues boucle sur i indiv *** puis GENOFILE et LOISELLE en bouclant sur k locus et j indiv
   //  freqall();//update a chaque generation
//freqall();
//freqallQuant();

      for(i=0;i<Npop;i++) { //float Qbarre = Qijtot();
       // *** edition GENOFILE boucle sur les k locus ***
         for(k=0;k<Nloc;k++) {
             if (pop[i]->Genotype[0][k]+1<10) genofile <<"0"<<pop[i]->Genotype[0][k]+1;
             else genofile << pop[i]->Genotype[0][k]+1;
             if (pop[i]->Genotype[1][k]+1<10) genofile <<"0"<<pop[i]->Genotype[1][k]+1<<"\t";
             else genofile <<pop[i]->Genotype[1][k]+1<<"\t"; //note: le +1 � la ligne du dessus permet de sortir des all�les qui ne sont pas consid�r�s commedonn�e manquante par Genetix ou autre
         }
         genofile << "\n"; //LG: saute une ligne entre chq generation
//**************Edition des genotypes quanti*********************************************
       genofileQuant<<"Indiv"<<i+1<<"\t";
       for(k=0;k<NlocQuant;k++) genofileQuant << pop[i]->GenotypeQuant[0][k]+1<<pop[i]->GenotypeQuant[1][k]+1<<"\t";
       genofileQuant << "\n";
      }
cout<<"OK5"<<endl;
//***************************************************************************************
 //fin de la generation N-1
//********************************************************************************************
     // *** edition Matrice loiselle DANS UN SOUS ECHANTILLON!!!, boucle sur les ixj indiv ***

}

void CurrentPop::PrintBreedVal() {

    for(int i=0;i<Npop;i++)
        {AddValues << acc+Npop+i+1 << "\t" << pop[i]->getbreedVal()<< "\t" << pop[i]->getphenVal() << endl;}
    AddValues <<"\n";

    }

// RAJOUT ECHANTILLONNAGE Avril 2012
void CurrentPop::Ech(boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tirage,
                             boost::math::gamma_distribution<>& gamma, int Ngroup) {
    int a,b,i,j,k;
    int motherlistF[Nfam*Noff];//liste des m�res
    int fatherlistF[Nfam*Noff];//liste des p�res
    Indiv* popF[Nfam*Noff];
    //int Ngroup=3; // Nobmre de m�res dans chaque groupe qui percoit les p�res de mani�re similaire
    int NgenPourFreq;

    std::vector <double> poids (Npop+1, 0.);
    std::vector <double> effetMat (Nfam+1, 0.);
    //dirichlet(Npop, poids, tirage, gamma);

    acc+=Npop;//compteur incr�ment� pour sortir les individus en num�rotation absolue


    for(i=0;i<(Nfam*Noff);i++) popF[i]=new Indiv;//nouvelle pop temporaire

   // popF= new Indiv*[Nfam*Noff];
   // for(i=0;i<Npop;i++)  popF[i]=new Indiv;
    for(b=0;b<Nfam;b++){   //boucle sur les a familles

        effetMat[b]= (random->nrand(0.,sigmaM));
        if (b%Ngroup==0) {dirichlet(Npop, poids, tirage, gamma);}

        for (a=0;a<Noff;a++){
            motherlistF[b*Noff+a]=b;//tirage d'une liste donnant les indices des m�res
            cout<<b<<"-"<<a<<"="<<b*Noff+a<<": "<<motherlistF[b*Noff+a]<<"\t";
            if (random->uniform()<s) fatherlistF[b*Noff+a]=motherlistF[b*Noff+a];//id pour p�res
            else {
                //do fatherlistF[b*Noff+a]=random->irand(Npop);   // peut etre rajouter une dirichlet?
                do fatherlistF[b*Noff+a]=draw_from_vec(poids,tirage);
                while (fatherlistF[b*Noff+a]==motherlistF[b*Noff+a]);     // evite de tirer meme pere meme mere
                }
                //cout<<i+a*(Npop/2)<<": \t"<<motherlist[i+a*(Npop/2)]<<"-"<<fatherlist[i+a*(Npop/2)]<<"\t";
        }
cout<<endl;
    }
cout<<"fini"<<endl;
    for (k=0;k<NlocQuant;k++){//on boucle sur les locus en premier
        for (i=0;i<(Nfam*Noff);i++) {//sur les individus
            popF[i]->SetGenotypesQuant(pop[motherlistF[i]]->GenotypeQuant[random->irand(2)][k],0,k);
            popF[i]->SetGenotypesQuant(pop[fatherlistF[i]]->GenotypeQuant[random->irand(2)][k],1,k);
        }
    }
cout<<"locquand ok"<<endl;
        for (k=0;k<Nloc;k++){//on boucle sur les locus en premier
            for (i=0;i<(Nfam*Noff);i++) {//sur les individus
               popF[i]->SetGenotypes(pop[motherlistF[i]]->Genotype[random->irand(2)][k],0,k);
               popF[i]->SetGenotypes(pop[fatherlistF[i]]->Genotype[random->irand(2)][k],1,k);
             //on fixe les all�les du g�ne k de l'ind i en fonction des all�les parentaux (un de ses chromosomes tir� au hasard)
            }
        }
        cout<<"loc ok"<<endl;
        for (i=0;i<(Nfam*Noff);i++) {
          popF[i]->Mere=pop[motherlistF[i]];
          popF[i]->Pere=pop[fatherlistF[i]];
          popF[i]->Indexfather=fatherlistF[i];
          popF[i]->Indexmother=motherlistF[i];
        }
        for(i=0;i<(Nfam*Noff);i++) pedigree << i+1+acc << "\t" << motherlistF[i]+acc-Npop+1 <<"\t"<< fatherlistF[i]+acc-Npop+1 <<endl;
      //  for (i=0;i<(Nfam*Noff);i++) popF[i]=popF[i];
        cout<<"tampon ok"<<endl;

        float addValue[(Nfam*Noff)];
        int l;
        for(i=0;i<(Nfam*Noff);i++) {
            addValue[i]=0;
            for(j=0;j<NlocQuant;j++) {
                for (l=0;l<2;l++) addValue[i]+=allelequant[j][popF[i]->GenotypeQuant[l][j]]->GetAllelicEffect();
                //popF[i]->SetbreedVal(addValue[i]);
                }

            //popF[i]->SetbreedVal(addValue[i]+effetMat[motherlistF[i]]);  // Si effet maternel inclus dans valeur g�n�tique
            popF[i]->SetbreedVal(addValue[i]);  // Si effet maternel non inclus dans valeur g�n�tique

            popF[i]->SetphenVal(popF[i]->getbreedVal() + effetMat[motherlistF[i]] + random->nrand(0.,sigmaE));  // Si effet maternel non inclus dans valeur g�n�tique
            phenodata << i+1 << "\t" << popF[i]->getphenVal() << endl;//impression pop de base
            //AddValues << i+1 <<"\t" << popF[i]->getbreedVal()<<endl;
        }
        phenodata <<"\n\n";
        //AddValues <<"\n";

                cout<<"pheno ok"<<endl;
        //****************************************************************************************
        // *** calcul des coeff d'apparentements entre individus i et j ***
        for (i=0;i<(Nfam*Noff);i++) {
          for(j=0;j<(Nfam*Noff);j++) {
              if (i!=j && j>i) {// on remplit ici les cases hors de la diagonale
              KinFinal[i][j]=0.25*(KinshipMatrix[popF[i]->Indexfather][popF[j]->Indexmother]+KinshipMatrix[popF[i]->Indexfather][popF[j]->Indexfather]+KinshipMatrix[popF[i]->Indexmother][popF[j]->Indexmother]+KinshipMatrix[popF[i]->Indexmother][popF[j]->Indexfather]);
              }
              else if (j<i)  KinFinal[i][j]= KinFinal[j][i];
              //cases de la diagonale
              else  KinFinal[i][j]=0.5*(1+KinshipMatrix[popF[i]->Indexfather][popF[i]->Indexmother]);
              //on sort le phi(i,i) qui vaut 1/2(1+Fi) Fi coeff d'inbreeding
              }
          }
        for (i=0;i<(Nfam*Noff);i++) {
          for(j=0;j<(Nfam*Noff);j++) {
              phiAfile << KinFinal[i][j]<<"\t";
		  phiAdense << KinFinal[i][j]<<"\t";
          }
			phiAfile <<"\n";
			phiAdense << "\n";
        }
cout<<"kin ok"<<endl;
        // *** edition PHENODATA et AddValues boucle sur i indiv *** puis GENOFILE et LOISELLE en bouclant sur k locus et j indiv
        //  freqall();//update a chaque generation

GlobalSum=0; // Debut FREQALL
nullall=0;
    for(k=0;k<Nloc;k++){//boucle sur les locus
         for(i=0;i<length;i++) FreqMatrix[k][i]=0;//on initialise les cases de Freq � 0  //SDM Freq devient FreqMatrix[k]
         for(i=0;i<(Nfam*Noff);i++) {//boucle sur les individus
             for(j=0;j<2;j++) {//boucle sur les chromosomes
                 FreqMatrix[k][popF[i]->Genotype[j][k]]++; //LG: on regarde l'indiv 1 � Npop au locus 1 � Nloc et au chromosome 1 ou 2
             }  //  et on ajoute 1 dans la case de FreqMatrix correspondant � l'allele port�
         }
         for (j=0; j<length; j++) {
             GlobalSum+=(FreqMatrix[k][j]/(2*(Nfam*Noff)))*(1-FreqMatrix[k][j]/(2*(Nfam*Noff))); //LG:c'est p(1-p)
             freqall << FreqMatrix[k][j] <<"\t";
         }
         freqall << "\n";
    }
    freqall << "\n";

cout<<"freqall ok"<<endl;

        for(i=0;i<(Nfam*Noff);i++) { //float Qbarre = Qijtot();
       // *** edition GENOFILE boucle sur les k locus ***
         for(k=0;k<Nloc;k++) {
             if (popF[i]->Genotype[0][k]+1<10) genofile <<"0"<<popF[i]->Genotype[0][k]+1;
             else genofile << popF[i]->Genotype[0][k]+1;
             if (popF[i]->Genotype[1][k]+1<10) genofile <<"0"<<popF[i]->Genotype[1][k]+1<<"\t";
             else genofile <<popF[i]->Genotype[1][k]+1<<"\t"; //note: le +1 � la ligne du dessus permet de sortir des all�les qui ne sont pas consid�r�s commedonn�e manquante par Genetix ou autre
         }
         genofile << "\n"; //LG: saute une ligne entre chq generation
//**************Edition des genotypes quanti*********************************************
       genofileQuant<<"Indiv"<<i+1<<"\t";
       for(k=0;k<NlocQuant;k++) genofileQuant << popF[i]->GenotypeQuant[0][k]+1<<popF[i]->GenotypeQuant[1][k]+1<<"\t";
       genofileQuant << "\n";
      }

//**************Calcul et export des apparentement de Loiselle pairwise entre g�notype diploides des semis**********************
cout<<"edit ok"<<endl;
    for(i=0;i<(Nfam*Noff);i++) {
         for(j=0;j<(Nfam*Noff);j++) {//impression de la matrice de Loiselle et d'une matrice pour test de Mantel
                int cmpt1,cmpt2,l;
                float sumlocus[Nloc];
                float sum=0;
                for(k=0;k<Nloc;k++) {
                    sumlocus[k]=0;
                    MargSum[k]=0;
                    for(l=0;l<length;l++) {  // SDM FreqMatrix[i].size() = length (le code doit �tre super optimise a cet endroit!
                        cmpt1=0;
                        cmpt2=0;
                        if (1-FreqMatrix[k][l]/(2*(Nfam*Noff))<0.000001) continue; //LG:sauter le reste de la boucle si l'allele est fix�
                        if (popF[i]->Genotype[0][k]==l) cmpt1++;
                        if (popF[i]->Genotype[1][k]==l) cmpt1++;
                        if (popF[j]->Genotype[0][k]==l) cmpt2++;
                        if (popF[j]->Genotype[1][k]==l) cmpt2++;
                        sumlocus[k]+=(cmpt1/2.-FreqMatrix[k][l]/(2*(Nfam*Noff)))*(cmpt2/2.-FreqMatrix[k][l]/(2*(Nfam*Noff)));
                        MargSum[k]+=((FreqMatrix[k][l]/(2*(Nfam*Noff)))*(1-FreqMatrix[k][l]/(2*(Nfam*Noff))))/(2*(Nfam*Noff)-1);
                    }
                    if (MargSum[k]!=0) sum+=sumlocus[k]+MargSum[k];//on teste si le locus i en question n'est pas fix�
                }
                loiselfile << sum/GlobalSum <<"\t";
         }
         loiselfile<<"\n";
     }
     loiselfile << "\n" <<endl;

//**************Calcul et export des apparentement de Loiselle pairwise entre contribution paternelles des semis**********************


for(i=0;i<(Nfam*Noff);i++) {//boucle sur les individus pour calculer le genotype du pere
    for(k=0;k<Nloc;k++) {

        int alm1=pop[motherlistF[i]]->Genotype[0][k];
        int alm2=pop[motherlistF[i]]->Genotype[1][k];

        int ald1=popF[i]->Genotype[0][k];
        int ald2=popF[i]->Genotype[1][k];

        if (ald1!=ald2 && ald1==alm1 && ald2!=alm2) {popF[i]->SetGenotypes(ald2,0,k);}
        if (ald1!=ald2 && ald1==alm2 && ald2!=alm1) {popF[i]->SetGenotypes(ald2,0,k);}
        if (ald1!=ald2 && ald1!=alm1 && ald2==alm2) {popF[i]->SetGenotypes(ald1,1,k);}
        if (ald1!=ald2 && ald1!=alm2 && ald2==alm1) {popF[i]->SetGenotypes(ald1,1,k);}

    }
}

// Calcul des fr�quences all�liques sur les graines directement
//GlobalSum=0; // Debut FREQALL sur les graines directement
//nullall=0;
//    for(k=0;k<Nloc;k++){//boucle sur les locus
//         for(i=0;i<length;i++) FreqMatrix[k][i]=0;//on initialise les cases de Freq � 0  //SDM Freq devient FreqMatrix[k]
//         for(i=0;i<(Nfam*Noff);i++) {//boucle sur les individus
//             for(j=0;j<2;j++) {//boucle sur les chromosomes
//                 FreqMatrix[k][popF[i]->Genotype[j][k]]++; //LG: on regarde l'indiv 1 � Npop au locus 1 � Nloc et au chromosome 1 ou 2
//             }  //  et on ajoute 1 dans la case de FreqMatrix correspondant � l'allele port�
//         }
//         for (j=0; j<length; j++) {
//             GlobalSum+=(FreqMatrix[k][j]/(2*(Nfam*Noff)))*(1-FreqMatrix[k][j]/(2*(Nfam*Noff))); //LG:c'est p(1-p)
//             freqall << FreqMatrix[k][j] <<"\t";
//         }
//         freqall << "\n";
//    }
//    freqall << "\n";
//NgenPourFreq = (2*(Nfam*Noff));
//cout<<"freqall ok"<<endl;


// Calcul des fr�quences all�liques sur les contributions paternelles
// GlobalSum=0; // Debut FREQALL sur les contributions paternelles
// nullall=0;
//    for(k=0;k<Nloc;k++){//boucle sur les locus
//          for(i=0;i<length;i++) FreqMatrix[k][i]=0;//on initialise les cases de Freq � 0  //SDM Freq devient FreqMatrix[k]
//          for(i=0;i<(Nfam*Noff);i++) {//boucle sur les individus
//              for(j=1;j<2;j++) {//boucle sur les chromosomes
//                  FreqMatrix[k][popF[i]->Genotype[j][k]]++; //LG: on regarde l'indiv 1 � Npop au locus 1 � Nloc et au chromosome 1 ou 2
//              }  //  et on ajoute 1 dans la case de FreqMatrix correspondant � l'allele port�
 //         }
//         for (j=0; j<length; j++) {
//             GlobalSum+=(FreqMatrix[k][j]/((Nfam*Noff)))*(1-FreqMatrix[k][j]/((Nfam*Noff))); //LG:c'est p(1-p)
//             freqall << FreqMatrix[k][j] <<"\t";
//         }
//         freqall << "\n";
//    }
//     freqall << "\n";
// NgenPourFreq = ((Nfam*Noff));
// cout<<"freqall ok"<<endl;



// Calcul des fr�quences all�liques sur les adultes qui g�n�rent la g�n�ration n
GlobalSum=0; // Debut FREQALL sur contributions paternelles
 nullall=0;
    for(k=0;k<Nloc;k++){//boucle sur les locus
          for(i=0;i<length;i++) FreqMatrix[k][i]=0;//on initialise les cases de Freq � 0  //SDM Freq devient FreqMatrix[k]
           for(i=0;i<Npop;i++) {//boucle sur les individus adultes
             for(j=0;j<2;j++) {//boucle sur les chromosomes
                 FreqMatrix[k][pop[i]->Genotype[j][k]]++; //LG: on regarde l'indiv 1 � Npop au locus 1 � Nloc et au chromosome 1 ou 2
             }  //  et on ajoute 1 dans la case de FreqMatrix correspondant � l'allele port�
          }
        for (j=0; j<length; j++) {
             GlobalSum+=(FreqMatrix[k][j]/(2*Npop))*(1-FreqMatrix[k][j]/(2*Npop)); //LG:c'est p(1-p)
        freqall << FreqMatrix[k][j] <<"\t";
         }
          freqall << "\n";
     }
    freqall << "\n";
NgenPourFreq = 2*Npop;
cout<<"freqall ok"<<endl;


// debut du calcul de Lois Paternel

cout<<"edit ok"<<endl;
    for(i=0;i<(Nfam*Noff);i++) {
         for(j=0;j<(Nfam*Noff);j++) {//impression de la matrice de Loiselle et d'une matrice pour test de Mantel
                int cmpt1,cmpt2,l;
                float sumlocus[Nloc];
                float sum=0;
                for(k=0;k<Nloc;k++) {
                    sumlocus[k]=0;
                    MargSum[k]=0;
                    for(l=0;l<length;l++) {  // SDM FreqMatrix[i].size() = length (le code doit �tre super optimise a cet endroit!
                        cmpt1=0;
                        cmpt2=0;
                        if (1-FreqMatrix[k][l]/(NgenPourFreq)<0.000001) continue; //LG:sauter le reste de la boucle si l'allele est fix�
                        if (popF[i]->Genotype[0][k]==l) cmpt1++;
                        if (popF[i]->Genotype[1][k]==l) cmpt1++;
                        if (popF[j]->Genotype[0][k]==l) cmpt2++;
                        if (popF[j]->Genotype[1][k]==l) cmpt2++;
                        sumlocus[k]+=(cmpt1/2.-FreqMatrix[k][l]/NgenPourFreq)*(cmpt2/2.-FreqMatrix[k][l]/NgenPourFreq);
                        MargSum[k]+=((FreqMatrix[k][l]/NgenPourFreq)*(1-FreqMatrix[k][l]/NgenPourFreq))/(NgenPourFreq-1);
                    }
                    if (MargSum[k]!=0) sum+=sumlocus[k]+MargSum[k];//on teste si le locus i en question n'est pas fix�
                }
                loiselpatfile << sum/GlobalSum <<"\t";
         }
         loiselpatfile<<"\n";
     }
     loiselpatfile << "\n" <<endl;


phiAfile.flush(); //SDM: j'ajoute pour le cas ou la matrice resterait dans la memoire
loiselfile.flush();
loiselpatfile.flush();
freqall.flush();
phenodata.flush();
AddValues.flush();
}
