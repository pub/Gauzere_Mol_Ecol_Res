/***************************************************
*Classe Indiv:
***************************************************/
#include<iostream>
#include "Indiv.hpp"


Indiv::Indiv() {
               Mere=0;
               Pere=0;
             //  this->Nloc=Nloc;
             //  this->NlocQuant=NlocQuant;
           //    this->NallQuant=NallQuant;
               breedVal=0; //?
               phenVal=0; //?
               Genotype[0]= new int[Nloc];
               Genotype[1]= new int[Nloc];
               GenotypeQuant[0]= new int[NlocQuant];
               GenotypeQuant[1]= new int[NlocQuant];
            //   AllelicEffect = new double [NlocQuant][NallQuant];
}

Indiv::~Indiv(){//on lib�re la m�moire allou�e pour les objets une fois qu'ils sont d�truits
                delete[] Genotype[0];
                delete[] Genotype[1];
                delete[] GenotypeQuant[0];
                delete[] GenotypeQuant[1];
}
int Indiv::Nloc=0; //on d�finit le membre statique Nloc pour allouer la m�moire n�cessaire
// � son stockage (bien que le membre statique ne d�pende en r�alit� pas de ce scope)
int Indiv::NlocQuant=0;

void Indiv::SetGenotypes(int allele,int chromosome,int locus){
     Genotype[chromosome][locus]=allele;
}
void Indiv::SetGenotypesQuant(int allele,int chromosome,int locus){
     GenotypeQuant[chromosome][locus]=allele;
}
/*int Indiv::getGenotypes(int chromosome, int locus){
     return Genotype[chromosome][locus];
 }*/

void Indiv::videtonsac(){//on pourra virer tout �a quand �a marchera
     for(int i=0;i<Nloc;i++) cout<<this<<" "<<i<<" "<<Genotype[0][i]<<" "<<Genotype[1][i]<<endl;
     for(int i=0;i<NlocQuant;i++) cout<<this<<" "<<i<<" "<<GenotypeQuant[0][i]<<" "<<GenotypeQuant[1][i]<<endl;
}

/* void Indiv::SetAllelicEffect(int locus,int allele){
     AllelicEffect[locus][allele]=random->nrand(0.,sigmaA);
} */
void Indiv::SetbreedVal(double value){
       breedVal=value;
}

double Indiv::getbreedVal(){
       return breedVal;
}

void Indiv::SetphenVal(double value){
       phenVal=value;
}
double Indiv::getphenVal(){
       return phenVal;
}
