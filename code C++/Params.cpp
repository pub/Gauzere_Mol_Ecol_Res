
/***************************************************
*Classe Init permet d'initialiser le jeu de donn�es*
*� partir duquel on va simuler.                     *
***************************************************/

#include<iostream>
#include<fstream>
#include <vector>
using namespace std;
#include "Params.hpp"
#include <cstdlib>

Params::Params(int argn, char** argv) {

     switch (argn) {
             case 1:           // pas d'argument: affichage de l'aide
                  usage();
                   //inutile de breaker
             case 2:
                  arguments=argv;
                  readfile(argv[1]);
                  checkparameters();
                  break;
          /*   case 10:           // bon nombre d'arguments: lecture et verification
                  arguments = argv;
                  readparameters();
                  checkparameters();
                  break;     */
             default:         // nombre incorrect d'arguments
                  cout<<"Ligne de commande incorrecte\n"<<endl;
                  exit(-1);
     }
}


int Params::round(float f) {
    return (int)(f + 0.5);
}

void Params::usage() {
     printf("*********\n");
     printf("[Simul] : usage\n");
     printf("Type [executable name] sigmaA sigmaE Npop Nloc s Ngenerations Ngeno NlocQuanti\n");
     printf("or [executable name] Parameter file\n");
     printf("\tsigmaA = standard deviation of allelic effects\n");
     printf("\tsigmaE = error standard deviation\n");
     printf("\tNlocQuant = number of quantitative loci (positive integer)\n");
    // printf("\tFreqAllQuant = frequency of allele A at quantitative locus\n");
     printf("\tNallQuant = number of alleles by locus quanti\n");
     printf("\tNpop = population size (positive integer)\n");
     printf("\tNfam = nb familles sampled (positive integer)\n");
     printf("\tNoff = nb offspring per familles sampled (positive integer)\n");
     printf("\tNloc = number of loci (positive integer)\n");
     printf("\ts = selfing rate (positive real or null)\n");
     printf("\tNgenerations = integer larger or equal to 1\n");
     printf("\tNall = number of alleles by locus (type N if all individuals have\n\tdifferent alleles in the base population)\n");
   //  printf("**********\n");
   //  printf("\t***************\n"); //allele freq for quant loci
     system("PAUSE");
     exit(1);

}
/*
// litparametres(): convertit les arguments de la ligne de commande
void Params::readparameters() {
     int i;
     float f,p;
     sscanf(arguments[1],"%f",&sigmaA);
     sscanf(arguments[2],"%f",&sigmaE);
     NlocQuant = atoi(arguments[3]);
     NallQuant = atoi(arguments[4]);
     Npop = atoi(arguments[5]);
     Nloc = atoi(arguments[6]);
     sscanf(arguments[7],"%f",&s);
     Ngeneration=atoi(arguments[8]);
     Ngeno=atoi(arguments[9]);
 // sscanf(arguments[10],"%f",&FreqAllQuant);
         for (i=0;i<Ngeno;i++) {
              cin>>f;
              Freq.push_back(f);
         //tant que le bon nombre de frequence n'est pas saisi, le programme attend
         }
         for (i=0;i<NallQuant;i++) {   //idem pour freq locus quanti
              cin>>p;
              FreqQuant.push_back(p);
         }
     }
*/
void Params::readfile(char* infile) {
     ifstream file(infile);
     if (!file.is_open()) {cerr<<"cannot open infile"<<endl;exit(-1);}
     next(file); file >> sigmaA;
     next(file); file >> sigmaE;
     next(file); file >> sigmaM;
     next(file); file >> NlocQuant;
     next(file); file >> NallQuant;
     next(file); file >> Npop;
     next(file); file >> Nfam;
     next(file); file >> Noff;
     next(file); file >> Nloc;
     next(file); file >> s;
     next(file); file >> Ngeneration;
     next(file); file >> Ngeno;
   //  next(file); file >> FreqAllQuant;
    // Freqloc.resize(Ngeno);//il faut faire �a sinon par d�faut il est vide!
     Freq.resize(Nloc,vector<float>(Ngeno));
     for(int i=0;i<Nloc;i++) {
        for(int j=0;j<Ngeno;j++) {
            next(file);
            file >> Freq[i][j];
        cout<<"locus "<<i<<"allele"<<j<<"freq"<<Freq[i][j]<<endl;
        }
     }

    // file.ignore(8192, '\n');
     FreqQuant.resize(NallQuant);
     for(int i=0;i<NallQuant;i++) {
         next(file);
         file >> FreqQuant[i];
     }
   //  for(int i=0;i<NallQuant;i++) cout<<FreqQuant[i]<<"\t";
     file.close();
}


void Params::next(ifstream& infile) {
     while (infile.get()!='#') if(infile.peek()==EOF){
            cerr<<"problem with parameter file format"<<endl;exit(-1);
     }
}

// verifie la validite des parametres
void Params::checkparameters() {
     int i,j;
     float somme=0;
     if (Npop>10000) {cerr<<"Error: population too big\n";exit(-1);}
     if (Npop<Nfam) {cerr<<"Error: too many families sampled\n";exit(-1);}
    // if (FreqAllQuant>1) {cerr<<"Error: Frequencies must be within[0-1]\n";exit(-1);}
     if (s<0 || s>1) {cerr<<"Error: incorrect selfing rate\n";exit(-1);}
     if (Ngeneration==0) {cerr<<"Error: Number of generations must be at least 1\n";exit(-1);}
     for(i=0;i<Nloc;i++) {
        for(j=0;j<Ngeno;j++) {
            if (Freq[i][j]<0||Freq[i][j]>1) {cerr<<"Error: Frequencies must be within[0-1]\n";exit(1);}
          //  if ((round(Freq[i][j]*Npop)-Freq[i][j]*Npop)>0.000001||(round(Freq[i][j]*Npop)-Freq[i][j]*Npop)<-0.000001) {cerr<<"erreur: frequences ne doivent pas exceder precision (1/Npop)"<<endl;exit(-1);}
            somme+=Freq[i][j];
        }
     }
     if ((Nloc-somme)>0.0001||(Nloc-somme)<-0.0001) {cerr<<"Error: Frequencies must sum to 1 \n"<<endl;exit(-1);}
}

void Params::save(ofstream& file) const {
     int i,j;
     file << "sigmaA;" << sigmaA << endl;
     file << "sigmaE;" << sigmaE << endl;
     file << "sigmaM;" << sigmaM << endl;
     file << "Number of loci quanti;" << NlocQuant << endl;
     file << "Number of alleles by locus quanti;" << NallQuant << endl;
     file << "Number of individuals;" << Npop << endl;
     file << "Number of families sampled;" << Nfam << endl;
     file << "Number of offspring per families sampled;" << Noff << endl;
     file << "Number of loci;" << Nloc << endl;
     file << "Selfing rate;" << s << endl;
     file << "Number of generations;" << Ngeneration << endl;
     file << "Number of alleles by locus;" << Ngeno << endl;
    // file << "freq A quanti #" << FreqAllQuant << endl;
     file << "Allele frequencies;";
     for (i=0;i<Nloc;i++) {
        file <<"(";
        for (j=0;j<Ngeno;j++) file << Freq[i][j]<<"_";
        file << ")"<<"\t";
     }
     file << endl;
     file << "Allele frequencies quanti;";
     for (unsigned int i=0;i<FreqQuant.size();i++) file << FreqQuant[i]<<"_";
}
