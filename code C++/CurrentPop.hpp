
#ifndef CURRENTPOP
#define CURRENTPOP

#include "Indiv.hpp"
#include "Locus.hpp"
#include "Random.hpp"
#include "Params.hpp"
#include<vector>    // SDM: je vire tous les vector contenus dans cette classe
#include <boost/math/distributions/gamma.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/math/special_functions/round.hpp>


using namespace std;


class CurrentPop {
	// SDM: je mets le default constructeur en prive pour verifier qu'il ne sert pas... si le compilateur se plaint de CurrentPop( ) is private ... in this context  �a viendra de l�
             CurrentPop();
      public:
             CurrentPop(int,int,int,int,float,int,vector<vector< float > > ,Random*,float,float ,float,int/*,float*/,vector<float> ); //le dernier float est pour FreqQuant
             ~CurrentPop();
             Indiv** pop;
             Indiv** popF;
             Locus*** allelequant;
             void Generation(boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tirage,
                             boost::math::gamma_distribution<>& gamma, int Ngroup);
             void Ech(boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& tirage,
                             boost::math::gamma_distribution<>& gamma, int Ngroup);
            void PrintBreedVal();

      private:

              int length;  // devient membre de la classe pour connaitre la taille des tableaux
              int lengthQuant;
              inline int round(float);
              float GlobalSum;   // SDM sum est sauvegarde pour pas avoir a recalculer les frequences
              float* MargSum;
              float Loiselle(int,int);
              float Ritland96(int,int);
              float APSbis(int,int);
              float GlobalSumQuant;
              float** FreqMatrixQuant;
              float* MargSumQuant;
              int Npop,Nfam,Noff,Nloc,acc,nullall,Ngeneration;
              int NlocQuant,NallQuant;
              float s,sigmaA,sigmaE,sigmaM;
			  float** FreqMatrix;
			  float** KinshipMatrix;
              float** tpKinshipMatrix;
			  float** KinFinal;
              Indiv** popCache;
              int g;


              ofstream genofileQuant, genofile, pedigree, phenodata, AddValues, loiselfile, loiselpatfile, freqall, phiAfile, phiAdense;
              Random* random;
              };


#endif
