
#ifndef PARAMS
#define PARAMS
#include<vector>
#include <iostream>


class Params {
   public:
          Params(int, char**);
          int Npop,Nfam,Noff,Nloc,Ngeno,Ngeneration,NlocQuant,NallQuant;
          float s,sigmaA,sigmaE, sigmaM/*,FreqAllQuant*/;
         // vector<float> Freqloc;
          vector<vector< float > > Freq;
          vector<float> FreqQuant;
          void save(ofstream&) const;

   private:
        char** arguments;
        void usage();
        void readparameters();
        void readfile(char*);
        void next(ifstream&);
        void checkparameters();
        int round(float);

        };


#endif
