#include<iostream>
using namespace std;
#include<fstream>
#include "Random.hpp"
#include<cmath>

Random::Random(){

        default_seed_1 = 123456789;
        default_seed_2 = 987654321;
        filename ="seeds.txt";

        FILE* file;

        if (!(file = fopen(filename,"r")) || fscanf(file,"%lf %lf",&S1,&S2) !=2) {
           S1 = default_seed_1;
           S2 = default_seed_2;
        }
        if (file) fclose(file);

}

       /* ifstream file7("seeds.txt");
        int S1=-1; S2=-1;
        if (file7.eof()) { cerr << "no seeds" << endl; exit(1); }
        file7 >>S1;
        if (file7.eof()) { cerr << "miss one seed" << endl; exit(1); }
        file7 >>S2;
        if (S1<0 || S2<0) { cerr << "invalid values for seeds" << endl; exit(1); }


        //stockage des graines dans l'objet
        this->S1=S1;
        this->S2=S2;
        }*/

Random::~Random() {
        FILE* file=fopen(filename,"w");
        if (!file) cerr<<"cannot open seed file\n";

        printSeeds(file);
        fclose(file);
}

double Random::uniform (){
       double          z, r;
       int             i;
       //printf("seeds  %f  %f\n",S1,S2);  //si besoin de savoir les seeds � chaque boucle

       r = S1 / 53668;
       i = (int) r;//prend la partie enti�re de r
       r = (double) i;//on remet le nombre en double pr�cision
       S1 = 40014 * (S1 - r * 53668) - r * 12211; // red�finition de la premi�re graine
       if (S1 < 0)
       S1 += 2147483563;
       r = S2 / 52774;
       i = (int) r;
       r = (double) i;
       S2 = 40692 * (S2 - r * 52774) - r * 3791;//red�finition de la deuxi�me graine
       if (S2 < 0)
       S2 += 2147483399;
       z = S1 - S2;
       if (z < 1)
       z += 2147483562;
       return (z * 4.656613e-10);
}

int Random::irand(int x) {
    return (int) (uniform()*x);
}

double Random::erand(double esperance) {
       double tp = 0.0;
       while (tp==0.0) tp = uniform();
       return ( -(esperance)*log(tp));
}


double Random::nrand(double mean,double std){
       const int NbTirages = 12; //augmenter pour une meilleure precision. 12 est bien.
                            //increase for better precision. 12 works fine.
       double valeur = 0;
       for(int i=0 ; i < NbTirages ; ++i) valeur += uniform();

       //on recentre la somme / centering the sum
       valeur -= NbTirages/2;

       //on etale suivant l'ecartype / spread with standard deviation
       //le 12 n'a rien a voir avec NbTirages, mais explique pourquoi justement, on prend souvent
       //NbTirages = 12
       //the 12 is not related to NbTirages, but it explains why it is often chosen that NbTirages=12
       valeur *= (NbTirages == 12) ? std
                              : sqrt(12/static_cast<double>(NbTirages))*std;
       //on centre sur la moyenne / debias
       valeur += mean;

       return valeur;
}



void Random::printSeeds(FILE* file) {
     fprintf(file,"%ld %ld\n\n",(long)S1,(long)S2);
}
