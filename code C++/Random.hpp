
#ifndef RANDOM
#define RANDOM

class Random {
public:
       Random();
       ~Random();
       double uniform();
       double erand(double);
       int irand(int);//pas besoin de drand ici on utilisera uniform
       double nrand(double,double);
       void printSeeds(FILE*);
       
       
private: 
       double  S1, S2;//les seeds qui servent � l'algorithme
       long default_seed_1;
       long default_seed_2;
       const char* filename;
       };
       
       
#endif
