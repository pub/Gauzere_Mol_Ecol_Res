
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <boost/math/distributions/gamma.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/math/special_functions/round.hpp>

using namespace std;
#include "Params.hpp"
#include "Random.hpp"
#include "Indiv.hpp"
#include "CurrentPop.hpp"
#include <cstdlib>



int main (int argn,char** argv) {
    int i,j;
    double alphaOld=5; // Pendant les générations initiales
    double alphaSampling=0.01;  // Pour la phase d'échantillonnage en dernière génération
    int NgroupOld=50;
    int NgroupSampling=3;

    Params params(argn,argv);
    Indiv::Nloc=params.Nloc;//attribution de la valeur du membre statique
    Indiv::NlocQuant=params.NlocQuant;
    Random random;
    Locus::sigmaA = params.sigmaA;
    Locus::random = &random;
    ofstream file ("paramfile.txt");
    if (!file.is_open()) {
        cerr<<"cannot open infile"<<endl;
        exit(-1);
    }
    params.save(file);
    file.close();

    boost::lagged_fibonacci19937 generator(static_cast<unsigned int>(std::time(0)));
    boost::uniform_real<> unif(0,1.0);
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> > tirage(generator, unif);
    boost::math::gamma_distribution<> gammaOld(alphaOld,1.);
    boost::math::gamma_distribution<> gammaSampling(alphaSampling,1.);


    CurrentPop simul(params.Ngeneration,params.Npop,params.Nfam,params.Noff,params.s,params.Nloc,params.Freq,&random,params.sigmaA,params.sigmaE, params.sigmaM ,params.NlocQuant,params.FreqQuant);
    j=1;
    while(j<params.Ngeneration) {
        cout<<"\nGeneration["<<j<<"]"<<endl;
        simul.Generation(tirage, gammaOld,NgroupOld);
        j++;
    }
    if(j==params.Ngeneration){
        cout<<"\nLast Generation["<<j<<"]"<<endl;
        simul.PrintBreedVal();
        simul.Ech(tirage, gammaSampling,NgroupSampling);
    }

    cout <<"Programme termine"<<endl;
    return 0;
}
