#ifndef INDIV
#define INDIV

using namespace std;



class Indiv {
      public:
             Indiv();
             ~Indiv();
             void SetGenotypes(int,int,int);
             void SetGenotypesQuant(int,int,int);
         //    void SetAllelicEffect(int,int);
             void SetbreedVal(double);
             void SetphenVal(double);
             double getbreedVal();//accesseurs
             double getphenVal();//accesseurs
             //int getGenotypes(int,int);
             int Indexfather;
             int Indexmother;
             int* Genotype[2];// mis public pour eviter de tr�s nombreux appels � un accesseur
             int* GenotypeQuant[2];
         //    double* AllelicEffect;
             Indiv* Mere;
             Indiv* Pere;
             void videtonsac();
             static int Nloc;
             static int NlocQuant;
         //    static int NallQuant;

      private:

             double breedVal;//la breeding value de chaque individu
             double phenVal;//valeur ph�no de l'individu
              };


#endif
